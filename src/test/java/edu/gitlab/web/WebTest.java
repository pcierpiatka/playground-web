package edu.gitlab.web;

import edu.gitlab.model.Model;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


class WebTest {

  @Test
  void test() {
    //given
    //when
    final Model model = new Web().make();
    // then

    Assertions.assertEquals(model.name, "this is sparta");
  }
}